#-------------------------------------------------
#
# Project created by QtCreator 2014-10-07T13:27:45
#
#-------------------------------------------------

QMAPWIDGET = ../QMapWidget
INCLUDEPATH += $$QMAPWIDGET

QT       += core gui network xml positioning

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QMapWidgetTest
TEMPLATE = app


SOURCES += main.cpp \
        $$QMAPWIDGET/qmapwidget.cpp \
        mainwindow.cpp \
    calculateroute.cpp

HEADERS  += mainwindow.h \
            $$QMAPWIDGET/qmapwidget.h \
            $$QMAPWIDGET/slippymap.h \
            $$QMAPWIDGET/gps.h \
            $$QMAPWIDGET/tilemap.h \
    calculateroute.h

FORMS    += mainwindow.ui \
    calculateroute.ui
