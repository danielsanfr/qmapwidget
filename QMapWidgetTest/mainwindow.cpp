/*************************************************************************
**
**  Copyright (C) 2014 Munteanu Marian
**
**  GNU General Public License Usage
** This file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "calculateroute.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    map = new QMapWidget(this);
    ui->setupUi(this);
    setCentralWidget(map);
    map->setFocus();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete map;
}

void MainWindow::on_actionQuit_triggered()
{
    this->close();
}

void MainWindow::on_actionCenterMap_triggered()
{
    if(map->hasPositioning())
        map->showCurrentPosition();
    else
        QMessageBox::warning(this, "GPS", "You don't have any active positioning services.");
}

void MainWindow::on_actionStart_GPS_triggered()
{
    map->startPositioning();
}

void MainWindow::on_actionStop_GPS_triggered()
{
    map->stopPositioning();
}

void MainWindow::on_actionGPS_Info_triggered()
{
    QMessageBox::information(this, "GPS Info", map->gpsInfo());
}

void MainWindow::on_actionCalculate_Route_triggered()
{
    CalculateRoute cr(this);
    if(cr.exec() == QDialog::Accepted)
    {
        if(cr.fLa!=0&&cr.fLo!=0&&cr.tLa!=0&&cr.tLo!=0)
        {
            map->showRoute(cr.fLa, cr.fLo, cr.tLa, cr.tLo);
        }
    }
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::information(this, "About", "A sample application that uses QMapWidget.\n\nhttp://marian.host.sk/qmapwidget");
}
