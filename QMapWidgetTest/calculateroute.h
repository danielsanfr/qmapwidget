/*************************************************************************
**
**  Copyright (C) 2014 Munteanu Marian
**
**  GNU General Public License Usage
** This file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**************************************************************************/

#ifndef CALCULATEROUTE_H
#define CALCULATEROUTE_H

#include <QDialog>

namespace Ui {
class CalculateRoute;
}

class CalculateRoute : public QDialog
{
    Q_OBJECT

public:
    double fLa, fLo, tLa, tLo;
    explicit CalculateRoute(QWidget *parent = 0);
    ~CalculateRoute();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::CalculateRoute *ui;
};

#endif // CALCULATEROUTE_H
