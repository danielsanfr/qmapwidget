/*************************************************************************
**
**  Copyright (C) 2014 Munteanu Marian
**
**  GNU General Public License Usage
** This file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**************************************************************************/

#include "calculateroute.h"
#include "ui_calculateroute.h"

CalculateRoute::CalculateRoute(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CalculateRoute)
{
    ui->setupUi(this);
}

CalculateRoute::~CalculateRoute()
{
    delete ui;
}

void CalculateRoute::on_buttonBox_accepted()
{
    fLa = ui->tbFromLat->text().toDouble();
    fLo = ui->tbFromLon->text().toDouble();
    tLa = ui->tbToLat->text().toDouble();
    tLo = ui->tbToLon->text().toDouble();
}

void CalculateRoute::on_buttonBox_rejected()
{

}
