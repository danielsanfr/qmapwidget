/*************************************************************************
**
**  Copyright (C) 2014 Munteanu Marian
**
**  GNU General Public License Usage
** This file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMapWidget>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionQuit_triggered();

    void on_actionCenterMap_triggered();

    void on_actionStart_GPS_triggered();

    void on_actionStop_GPS_triggered();

    void on_actionGPS_Info_triggered();

    void on_actionCalculate_Route_triggered();

    void on_actionAbout_triggered();

private:
    Ui::MainWindow *ui;
    QMapWidget * map;
};

#endif // MAINWINDOW_H
